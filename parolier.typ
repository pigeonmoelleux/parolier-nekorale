#set page(
    paper: "a4",
    flipped: true,
    margin: 35pt
)

#let make_titre(nom, soutitre) = {
    text(
        size: 30pt,
        style: "italic",
        box(heading(
            level: 2,
            text(style: "italic", nom)
        )) + if soutitre != none {
            h(1em);
            box(heading(level:3, outlined:false, text(size:0.8em)[(#soutitre)]))
        } else {},
    )
    v(-0.5em)
}

#let youtube(code) = {
    link("https://www.youtube.com/watch?v=" + code)[
        #image("resources/youtube_icon.png", height: 45pt)
    ]
}

#let incognito(code) = {
    link("https://yewtu.be/watch?v=" + code)[
        #image("resources/incognito_icon.png", height: 45pt)
    ]
}

#let liens(code) = {
    place(bottom + right)[
        #table(
            columns: 2,
            gutter: 15pt,
            stroke: none,
            youtube(code),
            incognito(code)
        )
    ]
}

#let chanson(
    titre,
    soutitre: none,
    colonnes: 2,
    size: 22pt,
    gutter: 4%,
    txt: true,
    code,
    paroles
) = {
    pagebreak(weak: true)

    make_titre(titre, soutitre)

    let options = (:)
    block(
        /* height: 70%, */
        columns(colonnes, gutter: gutter,
            text(size: size, if txt {
                let a = read(paroles).split("\n\n\n").map(c =>
                    c.split("\n\n").map(p =>
                        p.split("\n")
                    )
                )
                for c in a {
                    for p in c {
                        for l in p {
                            let m = l.match(regex("#(.*)=(.*)"))
                            if (m != none) {
                                options.insert(m.captures.at(0), eval(m.captures.at(1)))
                            } else {
                                set text(..options)
                                l
                                linebreak()
                            }
                        }
                        parbreak()
                    }
                    colbreak(weak:true)
                }
            } else {
                include paroles
            })
        )
    )

    liens(code)
}

#align(center)[
    #heading(
        level: 1,
        text("Parolier de la Nekorale", size: 36pt)
    )

    #v(30pt)

    #image("resources/logo2024.png", height: 375pt)
]

#pagebreak()

#set text(size: 20pt)

#align(center)[
   = Liste des chansons
]

#columns(3, gutter: 2%)[
    #outline(
        title: none,
        // title: "Liste des chansons",
        target: heading.where(level: 2, outlined: true)
    )
]

#chanson(
    "Aura",
    "EKOSQGKn5Cw",
    "chansons/aura.txt",
    size: 14pt,
    colonnes: 3,
    gutter: 2%
)

#chanson(
    "Bad Apple",
    "FtutLA63Cp8",
    "chansons/bad_apple.txt",
    size: 18.5pt,
)

#chanson(
    "Binks no sake",
    "cyGarA73Tcc",
    "chansons/binks_no_sake.txt",
    size: 22pt,
    soutitre: "One Piece"
)

#chanson(
    "Bye bye yesterday",
    "q8L4VhyfVxQ",
    "chansons/bye_bye_yesterday.txt",
    size: 22pt,
)

#chanson(
    "Le bon rhum de Binks",
    "dPiqD0UcViE",
    "chansons/bon_rhum_de_binks.txt",
    size: 16pt,
    colonnes: 3,
    gutter: 2%,
    soutitre: "One Piece"
)

#chanson(
    "Black Night Town",
    "lYoqAkd9d8s",
    "chansons/black_night_town.txt",
    soutitre: "Naruto",
    colonnes: 1,
    size: 22pt,
)

#chanson(
    "Blue Bird",
    "2upuBiEiXDk",
    "chansons/blue-bird.txt",
    soutitre: "Naruto",
    size: 20pt,
)

#chanson(
    "Deja Vu",
    "dv13gl0a-FA",
    "chansons/deja_vu.txt",
    size: 18pt,
    soutitre: "Initial D"
)

#chanson(
    "Diggy diggy hole",
    "ytWz0qVvBZ0",
    "chansons/diggy_diggy_hole.txt",
    size: 15.5pt,
    colonnes: 3
)

#chanson(
    "The dragonborn comes",
    "FOkkE93coIg",
    "chansons/dragonborn_comes.txt",
    soutitre: "Skyrim",
)

#chanson(
    "Edge of Dawn",
    "UQz-RAijEjg",
    "chansons/edge_of_dawn.txt",
    soutitre: "Fire Emblem: Three Houses",
    colonnes: 3,
    size: 14.5pt
)

#chanson(
    "Enemy",
    "IOrbP1OqNsg",
    "chansons/enemy.txt",
    soutitre: "Arcane",
    size: 13pt,
)

#chanson(
    "Générique Goldorak",
    "NfO-4Xgapq4",
    "chansons/goldorak.txt",
    size: 22pt,
)

#chanson(
    "Gurren Lagann opening",
    "FwgMxjhXkKo",
    "chansons/gurren_lagann.txt",
    size: 22pt,
)

#chanson(
    "Hacking to the gate",
    "TR3ma_60-m4",
    "chansons/hacking_to_the_gate.txt",
    size: 20pt
)

#chanson(
    "Hikaru Nara",
    "cWtgGTCAjYY",
    "chansons/hikaru_nara.txt",
    soutitre: "Your lie in April",
    size: 14pt
)

#chanson(
    "Hitori Janai",
    "8g4JCII8JLE",
    "chansons/hitori_janai.txt",
    soutitre: "Dragon Ball GT",
    size: 16.5pt,
)

#chanson(
    "Hoist the Colors",
    "1EyM2jcBTW0",
    "chansons/hoist_the_colors.txt",
    soutitre: "Pirates des Caraïbes",
    size: 18pt,
)

#chanson(
    "Honor for all",
    "l7hsVkYh9Ts",
    "chansons/honor_for_all.txt",
    soutitre: "Dishonored",
    size: 17.5pt
)

#chanson(
    "How far I'll go",
    "cPAbx5kgCJo",
    "chansons/how_far_Ill_go.txt",
    soutitre: "Moana/Vaiana",
    size: 16pt,
)

#chanson(
    "I'm still here",
    "MUZwblurraA",
    "chansons/Im_still_here.txt",
    soutitre: "Treasure Planet",
    size: 14pt
)

#chanson(
    "IDOL",
    "ZRtdQ81jPUQ",
    "chansons/IDOL.txt",
    soutitre: "Oshi no ko (YOASOBI)",
    size: 12pt,
    colonnes: 3,
    gutter: 2%
)

#chanson(
    "Inazuma Eleven",
    "1rciz5RyNx4",
    "chansons/inazuma_eleven.txt",
    size: 18pt,
)

#chanson(
    "Inspecteur Gadget",
    "SeE6dzk1tO0",
    "chansons/inspecteur_gadget.txt",
    size: 20pt,
)

#chanson(
    "Jump Up, Super Star!",
    "e9r5hx47kxM",
    "chansons/jump_up_superstar.txt",
    soutitre: "Super Mario Odyssey",
    size: 14pt,
    colonnes: 3,
    gutter: 2%
)

#chanson(
    "Kimi o Nosete",
    "gdpEnkcT7Io",
    "chansons/kimi_o_nosete.txt",
    soutitre: "Le château dans le ciel",
)

#chanson(
    "Legends never die",
    "4Q46xYqUwZQ",
    "chansons/legends_never_die.txt",
    soutitre: "League of Legends",
    size: 18.5pt
)

#chanson(
    "Lifelight (JP)",
    "qiI7wTGV6Go",
    "chansons/lifelight-jp.txt",
    soutitre: "Super Smash Bros. Ultimate",
    colonnes: 3,
    size: 18pt
)

#chanson(
    "Lifelight (EN)",
    "EhgDibw7vB4",
    "chansons/lifelight.txt",
    soutitre: "Super Smash Bros. Ultimate",
    size: 14pt,
)

#chanson(
    "Lost in Thoughts All Alone",
    "ENwFAmeWEYk",
    "chansons/lost_in_thoughts.txt",
    soutitre: "Fire Emblem Fates",
    size: 15pt,
)

#chanson(
    "Mon ancêtre Gurdil",
    "3TAQViG36QA",
    "chansons/mon_ancetre_gurdil.txt",
    soutitre: "Naheulband",
    size: 14pt,
)

#chanson(
    "Un Monde Sans Danger",
    "L8U-Fisq3Yk",
    "chansons/code_lyoko.txt",
    soutitre: "Code Lyoko",
    size: 20pt,
)

#chanson(
    "Must have been the wind",
    "17Kszzcmfv4",
    "chansons/must_have_been_the_wind.txt",
    soutitre: "Skyrim",
    size: 13pt,
    colonnes: 3,
    gutter: 2%
)


#chanson(
    "Les Mystérieuses Cités d'Or",
    "7Z25kW7ewEc",
    "chansons/mysterieuses_cites_d'or.txt",
    size: 25pt,
)

#chanson(
    "Never to Return",
    "T1k5SCCN1sk",
    "chansons/never_to_return.txt",
    soutitre: "Pyre",
    size: 21pt,
)

#chanson(
    "Nightmares never end",
    "5Z5I1ydmSUE",
    "chansons/nightmares_never_end.txt",
    soutitre: "Little Nightmares 2",
    size: 12.8pt,
    colonnes: 3,
    gutter: 2%
)

#chanson(
    "No se habla de Bruno",
    "wTi8yLyHeb8",
    "chansons/no_se_habla_de_bruno.typ",
    soutitre: "Encanto",
    size:14.3pt,
    txt: false,
)

#chanson(
    "Pokémon",
    "jVm1NbrXaXc",
    "chansons/pokemon.txt",
    size: 20pt,
)

#chanson(
    "Le Pudding à l'Arsenic",
    "dn2vyORpuyw",
    "chansons/pudding_a_l'arsenic.txt",
    size: 16pt,
    colonnes: 3
)

#chanson(
    "Rains of Castamere",
    "9vDL7AgLdYQ",
    "chansons/rains_of_castamere.txt",
    soutitre: "Games of Thrones",
    size: 23pt
)

#chanson(
    "Rightfully",
    "N301TAIEYy4",
    "chansons/rightfully.txt",
    soutitre: "Goblin Slayer",
    size: 22pt
)

#chanson(
    "Rock and Stone",
    "3N88MUdWxyQ",
    "chansons/rock_and_stone.txt",
    soutitre: "Deep Rock Galactic",
    size: 19pt,
)

#chanson(
    "Skull and Bones",
    "ob9zIqygSSc",
    "chansons/skull_and_bones.txt",
    size: 16pt,
)

#chanson(
    "Sleeping in the Cold Below",
    "bNNl8VOjbcY",
    "chansons/sleeping_in_the_cold_below.txt",
    soutitre: "Warframe",
    size: 17pt,
    colonnes: 3
)

#chanson(
    "Song of Durin",
    "uxfoa23skHg",
    "chansons/song_of_durin.txt",
    soutitre: "Clamavi de Profundis",
    size: 14.5pt,
)

#chanson(
    "Stand Proud",
    "dOQWNEv4_6U",
    "chansons/stand_proud.txt",
    soutitre: "Jojo's bizarre adventure",
    size: 23pt
)

#chanson(
    "Still Alive",
    "Y6ljFaKRTrI",
    "chansons/still_alive.txt",
    soutitre: "Portal",
    size: 14.5pt
)

#chanson(
    "Sur tes pas",
    "ZtqtBhBbWY8",
    "chansons/sur_tes_pas.txt",
    soutitre: "Wakfu",
    size: 18pt,
    gutter: 2%,
    colonnes: 3
)

#chanson(
    "Tabidachi no Uta",
    "Kbh_SAZN0eg",
    "chansons/tabidachi_no_uta.txt",
    soutitre: "Assassination Classroom",
    size: 17.5pt,
    colonnes: 3
)

#chanson(
    "Tonari no totoro",
    "VEsNt2r6eVs",
    "chansons/tonari_no_totoro.txt",
    soutitre: "Mon voisin Totoro",
    size: 20pt,
)

#chanson(
    "Toss a coin to your witcher",
    "waMkFIzvDpE",
    "chansons/toss_a_coin.txt",
    size: 20pt,
    colonnes: 3,
    gutter: 2%
)

#chanson(
    "Want You Gone",
    "dVVZaZ8yO6o",
    "chansons/want_you_gone.txt",
    soutitre: "Portal 2",
    size: 16pt,
)

#chanson(
    "Warriors",
    "fmI_Ndrxy14",
    "chansons/warriors.txt",
    soutitre: "League of Legends",
    size: 20pt,
)

#chanson(
    "We all lift together",
    "2yIELWjG8Tc",
    "chansons/we_all_lift_together.txt",
    soutitre: "Warframe",
    size: 23pt
)

#chanson(
    "We are",
    "xPyJ2XhSKOI",
    "chansons/we_are.txt",
    soutitre: "One Piece",
    size: 23pt
)

#chanson(
    "We don't talk about Bruno",
    "bvWRMAU6V-c",
    "chansons/we_dont_talk_about_bruno.typ",
    soutitre: "Encanto",
    txt: false,
    size: 15pt
)

#chanson(
    "Weight of the world",
    "ToBQY630PZE",
    "chansons/weight_of_the_world.txt",
    soutitre: "NieR:Automata",
    size: 15pt,
)
