#let pepa=blue
#let felix=green
#let dolores=red
#let camilo=olive
#let other=black
#let isabela=purple
#let mirabel=orange
#let rainbow=gradient.linear(dolores, mirabel, felix, camilo, pepa, isabela)

#set text(pepa)
[Pepa + #text(felix)[Felix]]\
We don't talk about Bruno, no, no, no!\
We don't talk about Bruno... but

It was my wedding day
#text(felix)[(It was our wedding day)]\
We were getting ready, and there wasn't a cloud in the sky\
#text(felix)[(No clouds allowed in the sky)]\
Bruno walks in with a mischievous grin #text(felix)[(Thunder!)]\
You telling this story, or am I?\
#text(felix)[(I'm sorry mi vida, go on)]

Bruno says "It looks like rain"
#text(felix)[(Why did he tell us?)]\
In doing so he floods my brain
#text(felix)[(Abuela, get the umbrellas)]\
Married in a hurricane\
#text(felix)[(What a joyous day but anyway)]\

#set text(gradient.linear(pepa, felix))
#box[We don't talk about Bruno, no, no, no!]\
#box[We don't talk about Bruno!] #text(dolores)[(hey)]

#set text(dolores)
[Dolores]\
Grew to live in fear of Bruno stuttering or stumbling\
I could always hear him sort of muttering and mumbling\
I associate him with the sound of falling sand, ch-ch-ch\
It's a heavy lift, with a gift so humbling\
Always left Abuela and the family fumbling\
Grappling with prophecies they couldn't understand\
Do you understand?

#set text(camilo)
[Camilo]\
A seven-foot frame, rats along his back\
When he calls your name it all fades to black\
Yeah, he sees your dreams and feasts on your screams
#text(rainbow,box[(hey!)])\

#set text(rainbow)
#box[We don't talk about Bruno, no, no, no!]\
#text(dolores)[(We don't talk about Bruno, no, no!)]\
#box[We don't talk about Bruno!]
#text(dolores)[(We don't talk about Bruno!)]

#set text(other)
He told me my fish would die, the next day: dead!
#text(rainbow,box[(no, no)])\
He told me I'd grow a gut, and just like he said...
#text(rainbow, box[(no, no)])\
He sait that all my hair would disappear\
Now look at my head
#text(rainbow, box[(no, no)])\
Your fate is sealed when your prophecy is read!

#set text(isabela)
[Isabela]\
He told me that the life of my dreams\
Would be promised, and someday be mine\
He told me that my power would grow\
Like the grapes that thrive on the vine\
#text(other)[(Oye, Mariano's on his way)]

#set text(dolores)
He told me that the man of my dreams\
Would be just out of reach, betrothed to another\
It's like I hear him now #text(isabela)[(Hey sis,)]\
#text(isabela)[I want not a sound out of you]
(it's like I can hear him now)\
I can hear him now

#set text(mirabel)
[Mirabel]\
Um, Bruno\
Yeah, about that Bruno\
I really need to know about Bruno\
Gimme the truth and the whole truth, Bruno\
#set text(camilo)
(Isabella, your boyfriend's here)\
#set text(rainbow)
#box[(Time for dinner!)]

#box[[Everyone together]]

#set text(camilo)
[Camilo x2]\
A seven-foot frame, rats along his back\
When he calls your name it all fades to black\
Yeah, he sees your dreams and feasts on your screams

#set text(pepa)
[Pepa + #text(felix)[Felix]]\
It was my wedding day
#text(felix)[(It was our wedding day)]\
We were getting ready, and there wasn't a cloud in the sky\
#text(felix)[(No clouds allowed in the sky)]\
Bruno walks in with a mischievous grin #text(felix)[(Thunder!)]\
You telling this story, or am I?\
#text(felix)[(I'm sorry mi vida, go on)]

Bruno says "It looks like rain"\
#text(felix)[(Why did he tell us?)]\
In doing so he floods my brain\
#text(felix)[(Abuela, get the umbrellas)]\
Married in a hurricane\
#text(felix)[(What a joyous day but anyway)]\

#set text(isabela)
[Isabela]\
He told me that the life of my dreams\
Would be promised, and someday be mine\
He told me that my power would grow\
Like the grapes that thrive on the vine, I'm fine\
He told me that the life of my dreams\
Would be promised, and someday be mine\
And I'm fine! And I'm fine!
And I'm fine! I'll be fine!

#set text(dolores)
[Dolores]\
Grew to live in fear of Bruno stuttering or stumbling\
I could always hear him sort of muttering and mumbling\
I associate him with the sound of falling sand, ch-ch-ch\
It's a heavy lift, with a gift so humbling\
Always left Abuela and the family fumbling\
Grappling with prophecies they couldn't understand\
Do you understand?\
He told me that the man of my dreams\
Would be just out of reach, betrothed to another, another\
And I'm fine! And I'm fine!\
And I'm fine! I'm fine!

#set text(rainbow)
#box[He's here!]

#box[Don't talk about Bruno, no!]\
#text(mirabel)[(Why did I talk about Bruno?)]\
#box[Not a word about Bruno!]\
#text(mirabel)[(I never should have brought up Bruno)]
