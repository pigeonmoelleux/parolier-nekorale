[Chorus]
You are the ocean's gray waves, destined to seek
Life beyond the shore just out of reach
Yet the waters ever change, flowing like time
The path is yours to climb

In the white light, a hand reaches through
A double-edged blade cuts your heart in two
Waking dreams fade away,
Embrace the brand-new day
Sing with me a song of birthrights and love
The light scatters to the sky above
Dawn breaks through the gloom, white as a bone
Lost in thoughts all alone

[Chorus]

Embrace the dark you call a home,
Gaze upon an empty, white throne
A legacy of lies,
A familiar disguise
Sing with me a song of conquest and fate
The black pillar cracks beneath its weight
Night breaks through the day, hard as a stone
Lost in thoughts all alone

The path you walk on belongs to destiny,
Just let it flow
All of your joy and your pain will fall like the tide,
Let it flow
Life is not just filled with happiness,
Nor sorrow
Even the thorn in your heart,
In time it may become a rose

[Chorus humming]

A burdened heart sinks into the ground
A veil falls away without a sound
Not day nor night, wrong nor right
For truth and peace you fight
Sing with me a song of silence and blood
The rain falls, but can't wash away the mud
Within my ancient heart dwells madness and pride
Can no one hear my cry

[Chorus]
You are the ocean's grey waves
