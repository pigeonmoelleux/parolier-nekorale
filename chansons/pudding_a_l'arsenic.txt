(instrumental)

Dans un grand bol de strychnine
Délayez de la morphine
Faites tiédir a la casserole
Un bon verre de pétrole...

- Ho Ho, je vais en mettre deux.

Quelques gouttes de ciguë
De la bave de sangsue
Un scorpion coupé très fin

- Et un peu de poivre en grains !
- Nooon !
- Ah? Bon...

(instrumental)

Emiettez votre arsenic
Dans un verre de narcotique
Deux cuillères de purgatif
Qu’on fait bouillir à feu vif...

- Ho Ho, je vais en mettre trois.

Dans un petit plat à part
Tiédir du sang de lézard
La valeur d’un dé à coudre

- Et un peu de sucre en poudre !
- NON !
- Ah? Bon.

(instrumental)

Vous versez la mort-aux-rats
Dans du venin de cobra
Pour adoucir le mélange
Pressez trois quartiers d’orange...

- Ho Ho, je vais en mettre un seul.

Décorez de fruits confits
Moisis dans du vert-de-gris
Tant que votre pâte est molle

- Et un peu de vitriol !
- NON... OUIIIIIIIII !!!
- Aaah... Je savais bien qu’ça serait bon.

(instrumental)

Le pudding à l’arsenic
Nous permet ce pronostic
Demain sur les bords du Nil
Que mangeront les crocodiles ?
DES GAU-LOIIIIIIS !
