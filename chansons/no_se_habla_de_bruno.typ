#let pepa=blue
#let felix=green
#let dolores=red
#let camilo=olive
#let other=black
#let isabela=purple
#let mirabel=orange
#let rainbow=gradient.linear(dolores, mirabel, felix, camilo, pepa, isabela)

#set text(pepa)
[Pepa + #text(felix)[Felix]]\
No se habla de Bruno, no, no, no\
No se habla de Bruno, mas

Justo en mi boda fue
#text(felix)[(En nuestra boda fue)]\
Todo estaba listo con un clima precioso esa vez\
#text(felix)[(Ninguna nube esa vez)]\
Bruno con voz misteriosa habló #text(felix)[(¡Trueno!)]\
¿Tú cuentas la historia o lo hago yo?\
#text(felix)[(Lo siento, mi vida, hazlo tú)]

"Veo que pronto lloverá"
#text(felix)[(¿Qué insinuaba?)]\
Sabrás que lo tomé muy mal\
#text(felix)[(Abuela, ten la sombrilla)]\
Boda en un huracán\
#text(felix)[(Fue un día feliz pero es verdad)]\

#set text(gradient.linear(pepa, felix))
#box[No se habla de Bruno, no, no, no]\
#box[No se habla de Bruno #text(dolores)[(hey)]]

#set text(dolores)
[Dolores]\
Miedo al ver a Bruno balbuceando y tropezando\
Siempre lo recuerdo murmurando y farfullando\
Su sonido es como la arena al resbalar, tss-tss-tss\
Raro el don de ir visualizando\
Deja a la abuela, como a todos, temblando\
Enfrentando profecías sin interpretar\
¿Quieres tú intentar?

#set text(camilo)
[Camilo]\
Terror en su faz, ratas por detrás\
Al oír tu nombre no hay marcha atrás\
Grita mientras tiembles al despertar #text(rainbow,box[(hey)])\

#set text(rainbow)
#box[No se habla de Bruno, no, no, no]\
#text(dolores)[(No se habla de Bruno, no)]\
#box[No se habla de Bruno]\
#text(dolores)[(No se habla de Bruno)]

#set text(other)
Él dijo mi pez se moriría, y ve #text(rainbow,box[(no, no)])\
Él dijo, "Serás panzón" y justo así fue #text(rainbow, box[(no, no)])\
Él dijo que me quedaría sin pelo y mírame bien #text(rainbow, box[(no, no)])\
Las profecías se cumplen cada vez

#set text(isabela)
[Isabela]\
Él vio en mí un destino gentil\
Una vida de ensueños vendrá\
Y que así el poder de mi don\
Como uvas, va a madurar\
#text(other)[(Oye, Mariano va a llegar)]

#set text(dolores)
Él vio en mí un amor imposible\
Pactado al fin, en lazos con otra\
Casi lo puedo oír #text(isabela)[(hey, tú)]\
#text(isabela)[De ti ni un sonido saldrá] (cual si lo pudiera oír)\
Yo lo puedo oír

#set text(mirabel)
[Mirabel]\
Oh, Bruno\
Sí, sobre Bruno\
Ya digan lo que sepan de Bruno\
Denme la pura verdad de Bruno\
#set text(camilo)
(Isabela, tu amor llegó)
#set text(rainbow)
#box[(¡A la mesa!)]

#box[[Everyone together]]

#set text(camilo)
[Camilo x2]\
Terror en su faz, ratas por detrás\
Al oír tu nombre no hay marcha atrás\
Grita mientras tiembles al despertar\

#set text(pepa)
[Pepa + #text(felix)[Felix]]\
Justo en mi boda fue\
#text(felix)[(En nuestra boda fue)]\
Todo estaba listo con un clima precioso esa vez\
#text(felix)[(Ninguna nube esa vez)]\
Bruno con voz misteriosa habló #text(felix)[(¡Trueno!)]\
¿Tú cuentas la historia o lo hago yo?\
#text(felix)[(Lo siento, mi vida, hazlo tú)]

"Veo que pronto lloverá"\
#text(felix)[(¿Qué insinuaba?)]\
Sabrás que lo tomé muy mal\
#text(felix)[(Abuela, ten la sombrilla)]\
Boda en un huracán\
#text(felix)[(Fue un día feliz pero es verdad)]\

#set text(isabela)
[Isabela]\
Él vio en mí un destino gentil\
Una vida de ensueños vendrá\
Y que así el poder de mi don\
Como uvas, va a madurar ¿Porque?\
Él vio en mí un destino gentil\
Una vida de ensueños vendrá\
¡Estoy Bien! ¡Estoy Bien!\
¡Estoy Bien! ¡Muy Bien!

#set text(dolores)
[Dolores]\
Miedo al ver a Bruno balbuceando y tropezando\
Siempre lo recuerdo murmurando y farfullando\
Su sonido es como la arena al resbalar, tss-tss-tss\
Raro el don de ir visualizando\
Deja a la abuela, como a todos, temblando\
Enfrentando profecías sin interpretar\
¿Quieres tú intentar?\
Él vio en mí un amor imposible\
Pactado al fin, en lazos con otra, con otra\
¡Estoy bien! ¡Estoy bien!\
¡Estoy bien! ¡Voy muy bien!

#set text(rainbow)
#box[¡Llegaron!]

#box[No hablemos de Bruno, no]\
#text(mirabel)[(¿Porqué hablé sobre Bruno?)]\
#box[No hay que hablar sobre Bruno]\
#text(mirabel)[(no debo nunca hablar de Bruno)]
